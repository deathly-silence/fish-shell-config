alias ll='gls -alh --color --group-directories-first'
alias cp="cp -iv"      # interactive, verbose
alias mv="mv -iv"      # interactive, verbose
alias grep="grep -ni --color=auto"   # ignore case
alias r="reset"
alias ".."="cd .."
alias "..."="cd ../.."
alias "...."="cd ../../.."
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias du='du -ch'
alias jsc="/System/Library/Frameworks/JavaScriptCore.framework/Versions/Current/Resources/jsc"

set -g -x GRAPHLAB_FILEIO_ALTERNATIVE_SSL_CERT_FILE '/Users/cq/anaconda/envs/dato-env/lib/python2.7/site-packages/certifi/cacert.pem'

set fish_greeting
set -x HOMEBREW_GITHUB_API_TOKEN 'fa029e8a9fda5fab4363bbc84dacce64d20407b6'
set -g -x PATH /usr/local/bin /usr/local/sbin $PATH
set -g -x PATH /usr/local/opt/coreutils/libexec/gnubin $PATH

set -U EDITOR vim
set -x SVN_EDITOR vim
set -x MYSQL_HOME /usr/local/mysql
set -x JAVA_HOME (/usr/libexec/java_home -v 1.8)
set -x ANDROID_HOME /Users/cq/softwares/sdk/android-sdk
set -x PLAY_HOME '/Users/cq/softwares/sdk/playframework'
set -x PATH $PATH $MYSQL_HOME/bin $ANDROID_HOME/tools $ANDROID_HOME/platform-tools $PLAY_HOME /Applications/MATLAB_R2015a.app/bin /Users/cq/anaconda/bin

# Paths to your tackle
set tacklebox_path ~/.tackle ~/.tacklebox

# Theme
#set tacklebox_theme entropy

# Which modules would you like to load? (modules can be found in ~/.tackle/modules/*)
# Custom modules may be added to ~/.tacklebox/modules/
# Example format: set tacklebox_modules virtualfish virtualhooks

# Which plugins would you like to enable? (plugins can be found in ~/.tackle/plugins/*)
# Custom plugins may be added to ~/.tacklebox/plugins/
# Example format: set tacklebox_plugins python extract

# Load Tacklebox configuration
. ~/.tacklebox/tacklebox.fish

set fish_function_path $fish_function_path "/usr/local/lib/python2.7/site-packages/powerline/bindings/fish"
powerline-setup

